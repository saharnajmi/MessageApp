# Technologies
* MVVM Architecture
* Retrofit
* Kotlin Coroutines
* Flow
* LiveData
* Data Binding
* Hilt
* Room
