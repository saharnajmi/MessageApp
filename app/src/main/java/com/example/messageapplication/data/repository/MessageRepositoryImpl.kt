package com.example.messageapplication.data.repository

import com.example.messageapplication.data.Result
import com.example.messageapplication.data.model.Messages
import com.example.messageapplication.data.repository.source.local.MessagesLocalDataSource
import com.example.messageapplication.data.repository.source.remote.MessagesRemoteDataSource
import kotlinx.coroutines.flow.Flow
import javax.inject.Inject

class MessageRepositoryImpl @Inject constructor(
    private val messageMessagesRemoteDataSource: MessagesRemoteDataSource,
    private val messageMessagesLocalDataSource: MessagesLocalDataSource
) : MessageRepository {

    override fun getRemoteMessages(): Flow<Result<Messages>> =
        messageMessagesRemoteDataSource.getRemoteMessages()

    override fun getLocalMessages(): Flow<List<Messages.Message>> =
        messageMessagesLocalDataSource.getLocalMessages()

    override fun getFavorite(): Flow<List<Messages.Message>> =
        messageMessagesLocalDataSource.getFavorite()

    override suspend fun deleteMessage(idList: List<String>) =
        messageMessagesLocalDataSource.deleteMessage(idList)

    override suspend fun saveMessages(message: List<Messages.Message>) =
        messageMessagesLocalDataSource.saveMessages(message)

    override suspend fun updateMessage(message: Messages.Message) =
        messageMessagesLocalDataSource.updateMessage(message)

    override suspend fun updateUnreadMessages(items: List<String>) =
        messageMessagesLocalDataSource.updateUnreadMessages(items)

    override fun getMessagesCount(): Flow<Int> =
        messageMessagesLocalDataSource.getMessagesCount()
}
