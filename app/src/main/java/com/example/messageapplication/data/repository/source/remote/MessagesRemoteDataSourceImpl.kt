package com.example.messageapplication.data.repository.source.remote

import com.example.messageapplication.data.Result
import com.example.messageapplication.data.model.Messages
import com.example.messageapplication.service.ApiService
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.catch
import kotlinx.coroutines.flow.flow
import kotlinx.coroutines.flow.flowOn
import javax.inject.Inject

class MessagesRemoteDataSourceImpl @Inject constructor(
    private val apiService: ApiService
) : MessagesRemoteDataSource {

    override fun getRemoteMessages(): Flow<Result<Messages>> = flow {
        emit(Result.Loading)
        emit(Result.Success(apiService.getMessages()))
    }.flowOn(Dispatchers.IO)
        .catch {
            emit(Result.Error(it))
        }
}
