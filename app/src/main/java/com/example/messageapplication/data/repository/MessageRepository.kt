package com.example.messageapplication.data.repository

import com.example.messageapplication.data.Result
import com.example.messageapplication.data.model.Messages
import kotlinx.coroutines.flow.Flow

interface MessageRepository {

    fun getRemoteMessages(): Flow<Result<Messages>>

    fun getLocalMessages(): Flow<List<Messages.Message>>

    fun getFavorite(): Flow<List<Messages.Message>>

    suspend fun deleteMessage(idList: List<String>)

    suspend fun saveMessages(message: List<Messages.Message>)

    suspend fun updateMessage(message: Messages.Message)

    suspend fun updateUnreadMessages(items: List<String>)

    fun getMessagesCount(): Flow<Int>
}
