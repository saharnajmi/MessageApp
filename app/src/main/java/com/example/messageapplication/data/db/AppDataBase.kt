package com.example.messageapplication.data.db

import androidx.room.Database
import androidx.room.RoomDatabase
import com.example.messageapplication.data.model.Messages
import com.example.messageapplication.data.repository.source.local.MessagesLocalDataSourceImpl

@Database(entities = [Messages.Message::class], version = 2)
abstract class AppDataBase : RoomDatabase() {
    abstract fun messageDao(): MessagesLocalDataSourceImpl
}