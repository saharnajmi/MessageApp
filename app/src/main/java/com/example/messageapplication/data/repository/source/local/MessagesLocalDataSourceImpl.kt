package com.example.messageapplication.data.repository.source.local

import androidx.room.*
import com.example.messageapplication.data.model.Messages
import kotlinx.coroutines.flow.Flow

@Dao
interface MessagesLocalDataSourceImpl : MessagesLocalDataSource {

    @Query("SELECT * FROM message ORDER BY unread ASC,id DESC")
    override fun getLocalMessages(): Flow<List<Messages.Message>>

    @Query("delete from message where id in (:idList)")
    override suspend fun deleteMessage(idList: List<String>)

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    override suspend fun saveMessages(messages: List<Messages.Message>)

    @Update
    override suspend fun updateMessage(message: Messages.Message)

    @Query("SELECT * FROM message WHERE isFavorite = 1")
    override fun getFavorite(): Flow<List<Messages.Message>>

    @Query("SELECT COUNT(*) FROM message")
    override fun getMessagesCount(): Flow<Int>

    @Query("UPDATE message SET unread=1 WHERE id in (:items) ")
    override suspend fun updateUnreadMessages(items: List<String>)
}
