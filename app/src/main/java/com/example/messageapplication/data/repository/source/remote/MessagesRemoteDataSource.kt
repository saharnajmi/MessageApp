package com.example.messageapplication.data.repository.source.remote

import com.example.messageapplication.data.Result
import com.example.messageapplication.data.model.Messages
import kotlinx.coroutines.flow.Flow

interface MessagesRemoteDataSource {
    fun getRemoteMessages(): Flow<Result<Messages>>
}