package com.example.messageapplication.data.model

import androidx.room.Entity
import androidx.room.PrimaryKey

data class Messages(
    val messages: List<Message>
) {
    @Entity(tableName = "message")
    data class Message(
        @PrimaryKey(autoGenerate = false)
        val id: String,
        val description: String,
        val image: String? = null,
        val title: String,
        var unread: Boolean,
        var expanded: Boolean = false,
        var isFavorite: Boolean = false
    )
}