package com.example.messageapplication.data.repository.source.local

import com.example.messageapplication.data.model.Messages
import kotlinx.coroutines.flow.Flow

interface MessagesLocalDataSource {

    fun getLocalMessages(): Flow<List<Messages.Message>>

    fun getFavorite(): Flow<List<Messages.Message>>

    fun getMessagesCount(): Flow<Int>

    suspend fun deleteMessage(idList: List<String>)

    suspend fun saveMessages(messages: List<Messages.Message>)

    suspend fun updateMessage(message: Messages.Message)

    suspend fun updateUnreadMessages(items: List<String>)

}

