package com.example.messageapplication.ui.main

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.widget.TextView
import androidx.activity.viewModels
import androidx.appcompat.app.AppCompatActivity
import androidx.viewpager.widget.ViewPager
import com.example.messageapplication.R
import com.example.messageapplication.databinding.ActivityMainBinding
import com.example.messageapplication.ui.adapter.ViewPagerAdapter
import dagger.hilt.android.AndroidEntryPoint


@AndroidEntryPoint
class MainActivity : AppCompatActivity() {
    private lateinit var binding: ActivityMainBinding
    private val viewModel: BaseViewModel by viewModels()
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)

        setupViewPager()

        val tabOne = LayoutInflater.from(this).inflate(R.layout.tab_one_layout, null)
        val tabTwo = LayoutInflater.from(this).inflate(R.layout.tab_two_layout, null)

        tabOne.findViewById<TextView>(R.id.title).text = "عمومی"
        tabTwo.findViewById<TextView>(R.id.title).text = "ذخیره\u200Cشده"
        val tabOneCount = tabOne.findViewById<TextView>(R.id.count)

        binding.tabLayout.getTabAt(0)?.customView = tabOne
        binding.tabLayout.getTabAt(1)?.customView = tabTwo

        viewModel.messagesCount.observe(this) { count ->
            if (count == 0) {
                tabOneCount.visibility = View.GONE
            } else {
                tabOneCount.visibility = View.VISIBLE
                tabOneCount.text = count.toString()
            }
        }
        binding.viewPager.addOnPageChangeListener(object : ViewPager.OnPageChangeListener {
            override fun onPageScrolled(
                position: Int,
                positionOffset: Float,
                positionOffsetPixels: Int
            ) {
            }

            override fun onPageSelected(position: Int) {
                if (position == 0)
                    tabOneCount.setBackgroundResource(R.drawable.bg_tab_selected)
                else
                    tabOneCount.setBackgroundResource(R.drawable.bg_tab_not_selected)
            }

            override fun onPageScrollStateChanged(state: Int) {}
        })
    }

    private fun setupViewPager() {
        binding.viewPager.adapter = ViewPagerAdapter(supportFragmentManager)
        binding.tabLayout.setupWithViewPager(binding.viewPager)
    }
}