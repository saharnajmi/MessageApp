package com.example.messageapplication.ui.main

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.messageapplication.data.model.Messages
import com.example.messageapplication.databinding.FragmentMainBinding
import dagger.hilt.android.AndroidEntryPoint


@AndroidEntryPoint
class MainFragment : Fragment(),
    MessageAdapter.MessageOnClickListener,
    MessageAdapter.MessageOnLongClickListener {

    private lateinit var binding: FragmentMainBinding
    lateinit var adapter: MessageAdapter
    private val viewModel: MainViewModel by viewModels()

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = FragmentMainBinding.inflate(layoutInflater)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        viewModel.messages.observe(viewLifecycleOwner) {
            if (it.isNotEmpty()) {
                //hide empty layout
                binding.emptyLayout.emptyStateRootView.visibility = View.GONE
                binding.recyclerMessage.visibility = View.VISIBLE

                //init recyclerView
                adapter = MessageAdapter(
                    messages = it,
                    onClick = this,
                    onLongClick = this,
                    isSelectEnabled = true
                )

                binding.recyclerMessage.layoutManager =
                    LinearLayoutManager(requireContext(), RecyclerView.VERTICAL, false)
                binding.recyclerMessage.adapter = adapter

            } else {
                //show empty layout
                binding.emptyLayout.emptyStateRootView.visibility = View.VISIBLE
                binding.recyclerMessage.visibility = View.GONE
            }
        }
    }

    override fun onLongClick() {
        binding.layoutDelete.visibility = View.VISIBLE

        //delete message
        binding.buttonDelete.setOnClickListener {
            viewModel.deleteMessage(adapter.itemsSelected)
            binding.layoutDelete.visibility = View.GONE
            adapter.clearItemSelected()
        }

        //cancel delete
        binding.buttonCancel.setOnClickListener {
            binding.layoutDelete.visibility = View.GONE
            adapter.clearItemSelected()
        }
    }

    override fun onFavoriteClick(message: Messages.Message) {
        viewModel.addFavorite(message)
        binding.layoutDelete.visibility = View.GONE
    }

    override fun onPause() {
        viewModel.updateUnReadMessages(adapter.itemsIdRead)
        super.onPause()
    }
}