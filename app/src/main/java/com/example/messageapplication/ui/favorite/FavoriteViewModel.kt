package com.example.messageapplication.ui.favorite

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.messageapplication.data.model.Messages
import com.example.messageapplication.data.repository.MessageRepositoryImpl
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class FavoriteViewModel @Inject constructor(private val messageRepository: MessageRepositoryImpl) :
    ViewModel() {

    private val _messages = MutableLiveData<List<Messages.Message>>()
    val messages: LiveData<List<Messages.Message>> = _messages

    init {
        getFavorites()
    }

    private fun getFavorites() {
        viewModelScope.launch {
            messageRepository.getFavorite().collect { result ->
                _messages.value = result
            }
        }
    }

    fun deleteFavorite(message: Messages.Message) {
        viewModelScope.launch {
            message.isFavorite = false
            messageRepository.updateMessage(message)
            getFavorites()
        }
    }

    fun updateUnReadMessages(items: List<String>) {
        viewModelScope.launch {
            messageRepository.updateUnreadMessages(items)
        }
    }
}