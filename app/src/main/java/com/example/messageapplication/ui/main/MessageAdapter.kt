package com.example.messageapplication.ui.main

import android.content.Intent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.example.messageapplication.R
import com.example.messageapplication.data.model.Messages
import com.example.messageapplication.databinding.ItemMessageBinding


class MessageAdapter(
    private val messages: List<Messages.Message>,
    private val onClick: MessageOnClickListener,
    private val onLongClick: MessageOnLongClickListener? = null,
    val isSelectEnabled: Boolean = false
) : RecyclerView.Adapter<MessageAdapter.Holder>() {

    private val selectedItemsMap = HashMap<String, Boolean>()
    var itemsSelected = mutableListOf<String>()
    var itemsIdRead = mutableListOf<String>()
    private var showCheckBox = false

    inner class Holder(private val binding: ItemMessageBinding) :
        RecyclerView.ViewHolder(binding.root) {
        fun bindView(message: Messages.Message) {
            binding.apply {
                val context = binding.root.context
                title.text = message.title

                if (message.expanded) {

                    expendLessLayout.expendLayout.visibility = View.GONE
                    expendMoreLayout.expendLayout.visibility = View.VISIBLE

                    expendMoreLayout.description.text = message.description
                    if (message.image != null) {
                        expendMoreLayout.cardImage.visibility = View.VISIBLE
                        Glide.with(context)
                            .load(message.image)
                            .into(expendMoreLayout.image)
                    } else {
                        expendMoreLayout.cardImage.visibility = View.GONE
                    }
                } else {

                    expendLessLayout.expendLayout.visibility = View.VISIBLE
                    expendMoreLayout.expendLayout.visibility = View.GONE

                    expendLessLayout.description.text = message.description
                    if (message.image != null) {
                        expendLessLayout.cardImage.visibility = View.VISIBLE
                        Glide.with(context)
                            .load(message.image)
                            .into(expendLessLayout.image)
                    } else {
                        expendLessLayout.cardImage.visibility = View.GONE
                    }
                }


                //background color read item
                if (message.unread)
                    mainLayout.setBackgroundColor(ContextCompat.getColor(context, R.color.gray))
                else
                    mainLayout.setBackgroundColor(ContextCompat.getColor(context, R.color.white))

                //favorite
                if (message.isFavorite)
                    binding.bookmark.setImageResource(R.drawable.ic_bookmarked)
                else
                    binding.bookmark.setImageResource(R.drawable.ic_not_bookmarked)

                //expanded change icon
                if (message.expanded)
                    binding.expand.setImageResource(R.drawable.ic_expand_less)
                else
                    binding.expand.setImageResource(R.drawable.ic_expand_more)

                //save checked item
                binding.check.isChecked = selectedItemsMap[messages[adapterPosition].id] ?: false

                if (showCheckBox) {
                    binding.check.visibility = View.VISIBLE
                } else {
                    binding.check.visibility = View.GONE
                }

                if (isSelectEnabled) {
                    binding.root.setOnLongClickListener {
                        showCheckBox = true
                        onLongClick?.onLongClick()
                        notifyDataSetChanged()
                        true
                    }
                }

                //item select
                binding.check.setOnClickListener {
                    addItemSelectedList(adapterPosition)
                }

                //favorite
                binding.bookmark.setOnClickListener {
                    onClick.onFavoriteClick(message)
                }

                //expand/collapse items
                binding.expand.setOnClickListener {
                    message.expanded = !message.expanded
                    message.unread = true
                    itemsIdRead.add(message.id)
                    notifyItemChanged(adapterPosition)
                }

                //share
                binding.share.setOnClickListener {
                    val intent = Intent()
                    intent.action = Intent.ACTION_SEND
                    intent.putExtra(Intent.EXTRA_TEXT, message.title)
                    intent.type = "text/plain"
                    context.startActivity(Intent.createChooser(intent, "Share To:"))
                }
            }
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): Holder =
        Holder(ItemMessageBinding.inflate(LayoutInflater.from(parent.context), parent, false))

    override fun onBindViewHolder(holder: Holder, position: Int) =
        holder.bindView(messages[position])


    private fun addItemSelectedList(position: Int) {
        val messageId = messages[position].id
        if (itemsSelected.contains(messageId)) {
            itemsSelected.remove(messageId)
            selectedItemsMap[messageId] = false
        } else {
            itemsSelected.add(messageId)
            selectedItemsMap[messageId] = true
        }
    }

    fun clearItemSelected() {
        itemsSelected.clear()
        showCheckBox = false
        selectedItemsMap.clear()
        notifyDataSetChanged()
    }

    override fun getItemCount(): Int = messages.size

    interface MessageOnClickListener {
        fun onFavoriteClick(message: Messages.Message)
    }

    interface MessageOnLongClickListener {
        fun onLongClick()
    }
}
