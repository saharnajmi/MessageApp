package com.example.messageapplication.ui.favorite

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.messageapplication.data.model.Messages
import com.example.messageapplication.databinding.FragmentFavoriteBinding
import com.example.messageapplication.ui.main.MessageAdapter
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class FavoriteFragment : Fragment(), MessageAdapter.MessageOnClickListener {
    private lateinit var binding: FragmentFavoriteBinding
    private val viewModel: FavoriteViewModel by viewModels()
    lateinit var adapter: MessageAdapter

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = FragmentFavoriteBinding.inflate(layoutInflater)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        adapter = MessageAdapter(messages = arrayListOf(), onClick = this)

        viewModel.messages.observe(viewLifecycleOwner) {
            if (it.isNotEmpty()) {
                //hide empty layout
                binding.emptyLayout.emptyStateRootView.visibility = View.GONE
                binding.recyclerFavorite.visibility = View.VISIBLE

                //init recyclerView
                adapter = MessageAdapter(messages = it, onClick = this)
                binding.recyclerFavorite.layoutManager =
                    LinearLayoutManager(requireContext(), RecyclerView.VERTICAL, false)
                binding.recyclerFavorite.adapter = adapter

            } else {
                //show empty layout
                binding.emptyLayout.emptyStateRootView.visibility = View.VISIBLE
                binding.recyclerFavorite.visibility = View.GONE
            }
        }
    }


    override fun onFavoriteClick(message: Messages.Message) {
        viewModel.deleteFavorite(message)
    }

    override fun onPause() {
        viewModel.updateUnReadMessages(adapter.itemsIdRead)
        super.onDestroy()
    }
}