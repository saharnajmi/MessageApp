package com.example.messageapplication.ui.adapter

import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentPagerAdapter
import com.example.messageapplication.ui.favorite.FavoriteFragment
import com.example.messageapplication.ui.main.MainFragment

class ViewPagerAdapter(
    fm: FragmentManager
) : FragmentPagerAdapter(fm, BEHAVIOR_RESUME_ONLY_CURRENT_FRAGMENT) {
    override fun getItem(position: Int): Fragment {
        return when (position) {
            0 -> MainFragment()
            1 -> FavoriteFragment()
            else -> MainFragment()
        }
    }

    override fun getCount(): Int {
        return 2
    }
}