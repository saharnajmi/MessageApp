package com.example.messageapplication.ui.main

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.messageapplication.data.repository.MessageRepositoryImpl
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class BaseViewModel @Inject constructor(
    private val messageRepository: MessageRepositoryImpl
) : ViewModel() {

    init {
        getMessagesCount()
    }

    private val _messagesCount = MutableLiveData<Int>()
    val messagesCount: LiveData<Int> = _messagesCount


    private fun getMessagesCount() {
        viewModelScope.launch {
            messageRepository.getMessagesCount().collect() {
                _messagesCount.value = it
            }
        }
    }
}
