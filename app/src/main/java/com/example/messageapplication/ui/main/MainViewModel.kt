package com.example.messageapplication.ui.main

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.messageapplication.data.Result
import com.example.messageapplication.data.model.Messages
import com.example.messageapplication.data.repository.MessageRepositoryImpl
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class MainViewModel @Inject constructor(private val messageRepository: MessageRepositoryImpl) :
    ViewModel() {
    private val _messages = MutableLiveData<List<Messages.Message>>()
    val messages: LiveData<List<Messages.Message>> = _messages

    init {
        saveMessages()
        getMessages()
    }

    private fun saveMessages() {
        viewModelScope.launch {
            messageRepository.getRemoteMessages().collect { result ->
                when (result) {
                    is Result.Success -> {
                        messageRepository.saveMessages(result.data.messages)
                    }
                    else -> Unit
                }
            }
        }
    }

    private fun getMessages() {
        viewModelScope.launch {
            messageRepository.getLocalMessages().collect { result ->
                _messages.value = result
            }
        }
    }

    fun deleteMessage(idList: List<String>) {
        viewModelScope.launch {
            messageRepository.deleteMessage(idList)
        }
    }

    fun addFavorite(message: Messages.Message) {
        viewModelScope.launch {
            message.isFavorite = !message.isFavorite
            messageRepository.updateMessage(message)
        }
    }

    fun updateUnReadMessages(items: List<String>) {
        viewModelScope.launch {
            messageRepository.updateUnreadMessages(items)
        }
    }
}