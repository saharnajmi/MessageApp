package com.example.messageapplication.service

import com.example.messageapplication.data.model.Messages
import retrofit2.http.GET

interface ApiService {
    @GET("729e846c-80db-4c52-8765-9a762078bc82")
    suspend fun getMessages(): Messages
}