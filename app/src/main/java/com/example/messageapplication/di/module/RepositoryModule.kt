package com.example.messageapplication.di.module

import android.content.Context
import androidx.room.Room
import com.example.messageapplication.common.Constants
import com.example.messageapplication.data.db.AppDataBase
import com.example.messageapplication.data.repository.MessageRepository
import com.example.messageapplication.data.repository.MessageRepositoryImpl
import com.example.messageapplication.data.repository.source.local.MessagesLocalDataSource
import com.example.messageapplication.data.repository.source.remote.MessagesRemoteDataSource
import com.example.messageapplication.data.repository.source.remote.MessagesRemoteDataSourceImpl
import com.example.messageapplication.service.ApiService
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.qualifiers.ApplicationContext
import dagger.hilt.components.SingletonComponent
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
object RepositoryModule {
    @Singleton
    @Provides
    fun provideApiService(): ApiService = Retrofit.Builder()
        .addConverterFactory(GsonConverterFactory.create())
        .baseUrl(Constants.BASE_URL)
        .build()
        .create(ApiService::class.java)

    @Singleton
    @Provides
    fun provideRemoteDataSource(apiService: ApiService): MessagesRemoteDataSource =
        MessagesRemoteDataSourceImpl(apiService)

    @Provides
    @Singleton
    fun provideMessageDataBase(@ApplicationContext context: Context): AppDataBase =
        Room.databaseBuilder(
            context,
            AppDataBase::class.java,
            "db_message"
        ).build()

    @Provides
    @Singleton
    fun provideMessageDao(appDataBase: AppDataBase): MessagesLocalDataSource =
        appDataBase.messageDao()

    @Singleton
    @Provides
    fun provideMessagesRepository(
        messagesRemoteDataSource: MessagesRemoteDataSource,
        messagesLocalDataSource: MessagesLocalDataSource
    ): MessageRepository =
        MessageRepositoryImpl(messagesRemoteDataSource, messagesLocalDataSource)
}